package com.dazone.crewemail.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.dazone.crewemail.BuildConfig;
import com.dazone.crewemail.R;
import com.dazone.crewemail.event.TurnOnNetWork;
import com.dazone.crewemail.utils.NetworkUtil;
import com.dazone.crewemail.utils.Prefs;
import com.dazone.crewemail.utils.Statics;
import com.dazone.crewemail.utils.Util;

import org.greenrobot.eventbus.EventBus;

import static com.dazone.crewemail.activities.BaseActivity.isPause;
import static com.dazone.crewemail.utils.Util.isAppInForeground;

/**
 * Created by maidinh on 18-Jun-18.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
      /*  boolean isPause = new Prefs().getBooleanValue(Statics.ISSHOWAPP, true);*/
        int status = NetworkUtil.getConnectivityStatusString(context);
        if (status == NetworkUtil.NETWORK_STAUS_WIFI || status == NetworkUtil.NETWORK_STATUS_MOBILE) {
            if (isAppInForeground(context)) {
                EventBus.getDefault().post(new TurnOnNetWork());
            }

        }else if(status==NetworkUtil.NETWORK_STATUS_NOT_CONNECTED){
            if (isAppInForeground(context)) {
                Toast.makeText(context, R.string.no_network_error, Toast.LENGTH_SHORT).show();
            }

        }
        Log.d("sssDebugisPause",isAppInForeground(context)+"");
       /* if (!"android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
           *//* if (status == NetworkUtil.NETWORK_STATUS_NOT_CONNECTED) {
                Log.d("ssssDe", "fdfd");
            } else if (status == NetworkUtil.NETWORK_STAUS_WIFI || status == NetworkUtil.NETWORK_STATUS_MOBILE) {
                EventBus.getDefault().post(new TurnOnNetWork());
            }*//*
            if (status == NetworkUtil.NETWORK_STAUS_WIFI || status == NetworkUtil.NETWORK_STATUS_MOBILE) {
                EventBus.getDefault().post(new TurnOnNetWork());
            }
        }*/
    }
}