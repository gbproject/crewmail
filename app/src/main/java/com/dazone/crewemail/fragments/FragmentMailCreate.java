package com.dazone.crewemail.fragments;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.dazone.crewemail.DaZoneApplication;
import com.dazone.crewemail.R;
import com.dazone.crewemail.Service.UploadFileToServer;
import com.dazone.crewemail.Service.fileUploadDownload;
import com.dazone.crewemail.View.tokenautocomplete.FilteredArrayAdapter;
import com.dazone.crewemail.View.tokenautocomplete.TokenCompleteTextView;
import com.dazone.crewemail.activities.ActivityMailCreate;
import com.dazone.crewemail.activities.ListEmailActivity;
import com.dazone.crewemail.activities.LoginActivity;
import com.dazone.crewemail.activities.OrganizationActivity;
import com.dazone.crewemail.adapter.AdapterMailCreateAttachLinear;
import com.dazone.crewemail.adapter.AdapterMailCreateFromSpiner;
import com.dazone.crewemail.customviews.AlertDialogView;
import com.dazone.crewemail.customviews.PersonCompleteView;
import com.dazone.crewemail.data.AccountData;
import com.dazone.crewemail.data.AttachData;
import com.dazone.crewemail.data.ErrorData;
import com.dazone.crewemail.data.EventRequest;
import com.dazone.crewemail.data.MailBoxData;
import com.dazone.crewemail.data.PersonData;
import com.dazone.crewemail.data.UserData;
import com.dazone.crewemail.database.OrganizationUserDBHelper;
import com.dazone.crewemail.event.CompleteTextView;
import com.dazone.crewemail.event.NewMailEvent;
import com.dazone.crewemail.event.SpaceEvent;
import com.dazone.crewemail.helper.MailHelper;
import com.dazone.crewemail.interfaces.BaseHTTPCallBack;
import com.dazone.crewemail.interfaces.OnGetAllOfUser;
import com.dazone.crewemail.interfaces.OnGetListOfMailAccount;
import com.dazone.crewemail.interfaces.OnMailDetailCallBack;
import com.dazone.crewemail.interfaces.pushlishProgressInterface;
import com.dazone.crewemail.utils.EmailBoxStatics;
import com.dazone.crewemail.utils.PreferenceUtilities;
import com.dazone.crewemail.utils.Prefs;
import com.dazone.crewemail.utils.Statics;
import com.dazone.crewemail.utils.StaticsBundle;
import com.dazone.crewemail.utils.Util;
import com.dazone.crewemail.webservices.HttpRequest;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.gson.Gson;
import com.nononsenseapps.filepicker.FilePickerActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static com.dazone.crewemail.activities.ActivityMailCreate.isBack;
import static com.dazone.crewemail.activities.ListEmailActivity.isSendMail;
import static com.dazone.crewemail.utils.Statics.FILE_PICKER_SELECT;


/**
 * Created by THANHTUNG on 21/12/2015.
 */
public class FragmentMailCreate extends BaseFragment implements pushlishProgressInterface, TokenCompleteTextView.TokenListener, OnGetAllOfUser, BaseHTTPCallBack, OnGetListOfMailAccount, View.OnClickListener, OnMailDetailCallBack {
    private String TAG = "FragmentMailCreate";
    private ImageButton imgDropDown, imgAddAttach;
    private LinearLayout linearMainBccAndCc, linearAttach;
    private EditText edtMailCreateSubject, edtMailCreateContent;
    private TextView txtMailCreateOriginalMessage;
    private PersonCompleteView edtMailCreateTo, edtMailCreateBcc, edtMailCreateCc;
    private Spinner spnMailFrom;
    private CheckBox chkMailCreateQuote;
    private WebView webMailCreateContent;
    private ArrayList<PersonData> people = new ArrayList<>();
    private ArrayAdapter<PersonData> adapter;
    private MailBoxData mailBoxData = new MailBoxData();
    private MailBoxData mailBoxDataNew = new MailBoxData();
    private MailBoxData dataCreate;
    private View v;
    private AdapterMailCreateFromSpiner adapterMailCreateFromSpiner;
    private ArrayList<AccountData> listUser = new ArrayList<>();
    private int task = 4;
    private boolean isSend = true;
    private int isUpload = 0;
    private int isFail = 0;
    private String className;
    private long MailNo;
    private LinearLayout lnCC, lnBCC;
    private ArrayList<AttachData> tp = new ArrayList<>();
    private String textCheckSpace = "";
    SpaceEvent spaceEvent;
    private ProgressBar mProgressBar;
    private TextView txtPercentage;
    private String fileName = "";
    private ProgressBar progress_bar;
    int countFile;
    public ArrayList<PersonData> listTmpTo = new ArrayList<>();
    public ArrayList<PersonData> listTmpCc = new ArrayList<>();
    public ArrayList<PersonData> listTmpBCC = new ArrayList<>();

    public static FragmentMailCreate newInstance(int task, MailBoxData data, String className, long MailNo) {
        Bundle args = new Bundle();
        args.putInt(StaticsBundle.BUNDLE_MAIL_DETAIL_TASK, task);
        args.putSerializable(StaticsBundle.BUNDLE_MAIL_CREATE_FROM_DETAIL, data);
        args.putString(StaticsBundle.BUNDLE_MAIL_BOX_CLASS_NAME, className);
        args.putLong(StaticsBundle.BUNDLE_MAIL_NO, MailNo);
        FragmentMailCreate fragment = new FragmentMailCreate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        Fresco.initialize(getActivity());


        EventBus.getDefault().register(this);
//        PersonData.getDepartmentAndUser(this);
        try {
            PersonData.getDepartmentAndUser(this);
            AccountData.getAllAccount(this);
            HttpRequest.getInstance().removeTempFile(null);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        if (Util.isNetworkAvailable()) {
//            OrganizationUserDBHelper.clearData();
//            HttpRequest.getInstance().getDepartment(getContext(),null);
//        }


        Bundle bundle = getArguments();
        if (bundle != null) {
            task = (bundle.getInt(StaticsBundle.BUNDLE_MAIL_DETAIL_TASK));
            dataCreate = (MailBoxData) bundle.getSerializable(StaticsBundle.BUNDLE_MAIL_CREATE_FROM_DETAIL);
            className = bundle.getString(StaticsBundle.BUNDLE_MAIL_BOX_CLASS_NAME);
            MailNo = bundle.getLong(StaticsBundle.BUNDLE_MAIL_NO);
            if (dataCreate != null) {
                setUpCreate(dataCreate, className);
            } else {
                setUpDraft(MailNo);
            }
        }
       /* List<PersonData> result;
        result = Util.getFromSharedPrefs(getContext());
        if (result != null) {
            for (PersonData personData : result) {
                if (personData.getFullName().equalsIgnoreCase(UserData.getUserInformation().getFullName())) {

                } else {
                    people.add(personData);
                }
            }
        }*/
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_mail_create, container, false);
        initControl(v);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initAdapter();
        initClick();
        if (task != 3)
            BindData(dataCreate);
        Log.d("DATA__TEST___??", dataCreate + "");
        if (dataCreate == null) {
            linearMainBccAndCc.setVisibility(View.GONE);
        } else {
            if (dataCreate.getListPersonDataTo().size() > 0 && dataCreate.getListPersonDataTo() != null) {
                if (dataCreate.getListPersonDataCc() != null && dataCreate.getListPersonDataCc().size() > 0) {
                    lnCC.setVisibility(View.VISIBLE);
                    if (dataCreate.getListPersonDataBcc() != null && dataCreate.getListPersonDataBcc().size() > 0) {
                        lnBCC.setVisibility(View.VISIBLE);
                    } else {
                        lnBCC.setVisibility(View.GONE);
                    }
                } else {
                    if (dataCreate.getListPersonDataBcc() != null && dataCreate.getListPersonDataBcc().size() > 0) {
                        lnBCC.setVisibility(View.VISIBLE);
                    } else {
                        lnBCC.setVisibility(View.GONE);
                    }
                    lnCC.setVisibility(View.GONE);
                }
            }
        }
        if (!TextUtils.isEmpty(DaZoneApplication.getInstance().getPrefs().getAccessToken().trim())) {
            // Get intent, action and MIME type
            Intent intent = getActivity().getIntent();
            String action = intent.getAction();
            String type = intent.getType();
            if (type != null) {
                final ArrayList<PersonData> arrayList = new ArrayList<>();

                Bundle bundle = intent.getExtras();
                progress_bar.setVisibility(View.VISIBLE);
                if (bundle != null) {
                    final String[] emails = bundle.getStringArray(Intent.EXTRA_EMAIL);
                    HttpRequest.getInstance().getDepartmentV2(new OnGetAllOfUser() {
                        @Override
                        public void onGetAllOfUserSuccess(ArrayList<PersonData> list) {
                     /*   String serverLink = HttpRequest.getInstance().sRootLink;
                        ArrayList<PersonData> result = OrganizationUserDBHelper.getAllOfOrganization(serverLink);*/
                            progress_bar.setVisibility(View.GONE);
                            if (list.size() >= 0) {
                                Log.d("result", list.toString());
                                if (emails != null && emails.length >= 1) {
                                    for (int i = 0; i <= emails.length - 1; i++) {
                                        if (getPersonDataWithQueryUserNo(emails[i], arrayList, list).size() >= 1) {
                                            for (int j = 0; j <= getPersonDataWithQueryUserNo(emails[i], arrayList, list).size() - 1; j++) {
                                                edtMailCreateTo.addObject(getPersonDataWithQueryUserNo(emails[i], arrayList, list).get(j));
                                            }
                                        } else {
                                            PersonData P = new PersonData();
                                            P.setEmail(emails[i]);
                                            P.setFullName(emails[i]);
                                            edtMailCreateTo.addObject(P);
                                        }

                                    }
                                    progress_bar.setVisibility(View.GONE);
                                    mailBoxData.getListPersonDataTo().addAll(edtMailCreateTo.getObjects());
                                }
                            } else {
                                for (int i = 0; i < emails.length - 1; i++) {
                                    PersonData P = new PersonData();
                                    P.setEmail(emails[i]);
                                    P.setFullName("a");
                                    edtMailCreateTo.addObject(P);
                                }
                                mailBoxData.getListPersonData().addAll(edtMailCreateTo.getObjects());
                            }
                        }

                        @Override
                        public void onGetAllOfUserFail(ErrorData errorData) {
                            Log.d("errorData", errorData.toString() + "");
                            progress_bar.setVisibility(View.GONE);
                        }

                    });
                }


                // }
            }
            if (Intent.ACTION_SEND.equals(action) && type != null) {
                if ("text/plain".equals(type)) {
                    handleSendText(intent); // Handle text being sent
                } else {
                    handleSendImage(intent); // Handle single image being sent
                }
            } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
                // if (type.startsWith("image/") || type.startsWith("video/") || type.startsWith("audio/"))  {
                handleSendMultipleImages(intent); // Handle multiple images being sent
                // }
            } else {
                // Handle other intents, such as being started from the home screen

            }
        } else {
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            ActivityMailCreate.Instance.finish();
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }


    private ArrayList<PersonData> getPersonDataWithQueryUserNo(String query, ArrayList<PersonData> searchResultList, ArrayList<PersonData> searchList) {
        if (searchList != null) {
            for (PersonData personData : searchList) {
                if (personData.getType() == 2) {
                    if ((personData.getUserNo() >= 0 && personData.getmEmail().equals(query))) {
                        boolean isAdd = true;
                        for (PersonData userAdded : searchResultList) {
                            if (userAdded.getUserNo() == personData.getUserNo()) {
                                isAdd = false;
                            }
                        }
                        if (isAdd) {
                            searchResultList.add(personData);
                        }
                    }
                }
                if (personData.getPersonList() != null && personData.getPersonList().size() > 0) {
                    getPersonDataWithQueryUserNo(query, searchResultList, personData.getPersonList());
                }
            }
        }
        return searchResultList;
    }

    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            // Update UI to reflect text being shared
            try {

                edtMailCreateContent.setText(sharedText);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    void handleSendImage(Intent intent) {
        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            // Update UI to reflect image being shared
            GetFile(imageUri);
        }
    }

    void handleSendMultipleImages(Intent intent) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (imageUris != null) {
            // Update UI to reflect multiple images being shared
            for (Uri path : imageUris) {
                // Do something with the URI
                GetFile(path);
            }
        }
    }

    private void initControl(View v) {
        imgDropDown = (ImageButton) v.findViewById(R.id.imgMailCreateImageDropDown);
        lnCC = (LinearLayout) v.findViewById(R.id.ln_mail_cc);
        lnBCC = (LinearLayout) v.findViewById(R.id.ln_mail_bcc);
        progress_bar = (ProgressBar) v.findViewById(R.id.progress_bar);
        imgAddAttach = (ImageButton) v.findViewById(R.id.imgMailCreateAttach);
        linearMainBccAndCc = (LinearLayout) v.findViewById(R.id.linear_main_bcc_cc);
        linearAttach = (LinearLayout) v.findViewById(R.id.linear_attach);
        edtMailCreateTo =
                (PersonCompleteView) v.findViewById(R.id.edtMailCreateTo);
        edtMailCreateTo.setType(0);

       /* edtMailCreateTo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("beforeTextChanged", charSequence + "");
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String c = String.valueOf(charSequence);
                if (!c.trim().isEmpty() && c.trim().equals(textCheckSpace)) {
                    TokenCompleteTextView.spaceEvent();
                } else {
                    textCheckSpace = c.trim();
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.d("after TextChanged", editable.toString() + "");
            }
        });*/
        edtMailCreateCc =
                (PersonCompleteView) v.findViewById(R.id.edtMailCreateCc);
        edtMailCreateCc.setType(1);
        edtMailCreateBcc =
                (PersonCompleteView) v.findViewById(R.id.edtMailCreateBcc);
        edtMailCreateBcc.setType(2);


        edtMailCreateSubject = (EditText) v.findViewById(R.id.edtMailCreateSubject);
        edtMailCreateContent = (EditText) v.findViewById(R.id.edtMailCreateContent);
        spnMailFrom = (Spinner) v.findViewById(R.id.spnMailCreateAccountFrom);
        chkMailCreateQuote = (CheckBox) v.findViewById(R.id.chkMailCreateCheck);
        webMailCreateContent = (WebView) v.findViewById(R.id.webMailCreateContent);
        txtMailCreateOriginalMessage = (TextView) v.findViewById(R.id.txtMailCreateOriginalMessage);
        txtPercentage = (TextView) v.findViewById(R.id.txtPercentage);
        mProgressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        v.findViewById(R.id.imgMailCreateOrgTo).setOnClickListener(this);
        v.findViewById(R.id.imgMailCreateOrgCc).setOnClickListener(this);
        v.findViewById(R.id.imgMailCreateOrgBcc).setOnClickListener(this);
    }

    private void initAdapter() {
        setAdapter(edtMailCreateTo);
        setAdapter(edtMailCreateBcc);
        setAdapter(edtMailCreateCc);
        adapterMailCreateFromSpiner = new AdapterMailCreateFromSpiner(getActivity(), R.layout.fragment_mail_create_item_spiner_address_to, listUser);
        spnMailFrom.setAdapter(adapterMailCreateFromSpiner);
    }

    private void setAdapter(TokenCompleteTextView tokenCompleteTextView) {
        adapter = new FilteredArrayAdapter<PersonData>(getActivity(), R.layout.person_layout, people) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {

                    LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    convertView = l.inflate(R.layout.person_layout, parent, false);
                }
                PersonData p = getItem(position);
                ((TextView) convertView.findViewById(R.id.name)).setText(p.getFullName());
                ((TextView) convertView.findViewById(R.id.email)).setText(p.getmEmail());
                return convertView;
            }

            @Override
            protected boolean keepObject(PersonData person, String mask) {
                mask = mask.toLowerCase();
                return person.getFullName().toLowerCase().contains(mask) || person.getFullName().toLowerCase().contains(mask);
            }
        };
        tokenCompleteTextView.setAdapter(adapter);
        tokenCompleteTextView.setTokenListener(this);
        tokenCompleteTextView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Select);
    }

    private void initClick() {

        imgDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (linearMainBccAndCc.getVisibility() == View.VISIBLE) {
                    imgDropDown.setImageResource(R.drawable.dropdow_arr_ic);
                    linearMainBccAndCc.setVisibility(View.GONE);
                    lnCC.setVisibility(View.VISIBLE);
                    lnBCC.setVisibility(View.VISIBLE);
                } else {
                    imgDropDown.setImageResource(R.drawable.dropdow_arr_ic_02);
                    linearMainBccAndCc.setVisibility(View.VISIBLE);
                    lnCC.setVisibility(View.VISIBLE);
                    lnBCC.setVisibility(View.VISIBLE);
                }
            }
        });

        imgAddAttach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MailHelper.LoadFile(FragmentMailCreate.this);

             /*   Intent i = new Intent(getActivity(), FilePickerActivity.class);
                i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, true);
                i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
                i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);
                i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());

                startActivityForResult(i, FILE_PICKER_SELECT);*/
                browseClick();
                //AlertDialogView.normalAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.string_alert_update), null);
            }
        });

        spnMailFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PersonData personData = new PersonData();
                personData.setFullName(listUser.get(position).getName());
                personData.setEmail(listUser.get(position).getMailAddress());
                mailBoxData.setMailFrom(personData);
                mailBoxData.setUserNo(listUser.get(position).getAccountNo());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        chkMailCreateQuote.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    webMailCreateContent.setVisibility(View.VISIBLE);
                    txtMailCreateOriginalMessage.setVisibility(View.VISIBLE);
                } else {
                    webMailCreateContent.setVisibility(View.GONE);
                    txtMailCreateOriginalMessage.setVisibility(View.GONE);
                }
            }
        });

    }

    public void browseClick() {
        if (checkPermissionsReadExternalStorage()) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            intent.addCategory(Intent.CATEGORY_OPENABLE);

            //intent.putExtra("browseCoa", itemToBrowse);
            //Intent chooser = Intent.createChooser(intent, "Select a File to Upload");
            try {
                //startActivityForResult(chooser, FILE_SELECT_CODE);
                startActivityForResult(Intent.createChooser(intent, ""), FILE_PICKER_SELECT);
            } catch (Exception ex) {
                System.out.println("browseClick :" + ex);//android.content.ActivityNotFoundException ex
            }
        } else {
            setPermissionsReadExternalStorage();
        }
    }

    private void setUpCreate(MailBoxData data, String className) {
        ArrayList<PersonData> temp = new ArrayList<>();
        ArrayList<PersonData> temp2 = new ArrayList<>();
        ArrayList<PersonData> temp3 = new ArrayList<>();
        mailBoxData.setMailNo(data.getMailNo());
        mailBoxData.setContentReply(data.getContentReply());
        if (task == 0) {
            mailBoxData.setSubject("FWD: " + data.getSubject());
            if (data.getListAttachMent() != null) {
                mailBoxData.setListAttachMent(data.getListAttachMent());
                Log.d("sssDebugsssDebug", mailBoxData.toString() + "");
                tp.addAll(data.getListAttachMent());
            }
        } else {
            if (EmailBoxStatics.MAIL_CLASS_OUT_BOX.equals(className)) {
                // out box
                mailBoxData.setListPersonDataTo(data.getListPersonDataTo());
                mailBoxData.setSubject(data.getSubject());
                mailBoxData.setListPersonDataCc(data.getListPersonDataCc());
                mailBoxData.setListPersonDataBcc(data.getListPersonDataBcc());
            } else {
                // other mail box
                temp.add(data.getMailFrom());
                mailBoxData.setSubject("RE: " + data.getSubject());
                mailBoxData.setListPersonDataTo(temp);
            }
            if (task == 2) {
                temp.addAll(MailHelper.removeMe(data.getListPersonDataTo()));
                mailBoxData.setListPersonDataTo(temp);
                temp2.addAll(MailHelper.removeMe(data.getListPersonDataCc()));
                mailBoxData.setListPersonDataCc(temp2);
                temp3.addAll(MailHelper.removeMe(data.getListPersonDataBcc()));
                mailBoxData.setListPersonDataBcc(temp3);
            }
        }
    }

    private void setUpDraft(long mailNo) {
        if (task == 3) {
            HttpRequest.getInstance().getMaillDetail(this, mailNo);
        }
    }

    AttachData attachData;

    public void GetFile(Uri uri) {
        // String Path = Util.getPathFromURI(uri, getActivity());
        String Path = getPath(getActivity(), uri);
        String fileName = Util.getFileName(uri, getActivity());
        String fileType = fileName.substring(fileName.lastIndexOf("."));
        long fileSize = Util.getFileSize(Path);
        if (linearAttach != null) {
            linearAttach.setVisibility(View.VISIBLE);
        }
        attachData = new AttachData(fileName, Path, fileSize, fileType, System.currentTimeMillis());
        mailBoxData.getListAttachMent().add(attachData);
        final AdapterMailCreateAttachLinear itemView = new AdapterMailCreateAttachLinear(getActivity(), attachData);
        itemView.setTag(attachData);
        itemView.getImageButtonDelete().setVisibility(View.VISIBLE);
        itemView.getImageButtonDelete().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mailBoxData.getListAttachMent().remove(itemView.getTag());
                linearAttach.removeView(itemView);
                isUpload--;
                updateTextView();
                if (mailBoxData.getListAttachMent().size() == 0) {
                    attachData = null;
                    mProgressBar.setVisibility(View.GONE);
                    if (progress_bar != null) {
                        progress_bar.setVisibility(View.GONE);
                    }
                    txtPercentage.setVisibility(View.GONE);
                }
                Util.printLogs("Còn mấy file: " + mailBoxData.getListAttachMent().size());
            }
        });
        itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return false;
            }
        });
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MailHelper.OpenFile(getActivity(), (AttachData) itemView.getTag());
            }
        });
        linearAttach.addView(itemView);

        new Upload().execute(attachData.getPath(), attachData.getFileName());
    }

    public void handleSelectedOrganizationResult(int type, ArrayList<PersonData> resultList) {
//        if (type == Statics.ORGANIZATION_TO_ACTIVITY) {
//            edtMailCreateTo.clear();
//        }
//        if (type == Statics.ORGANIZATION_CC_ACTIVITY) edtMailCreateCc.clear();
//        if (type == Statics.ORGANIZATION_BCC_ACTIVITY) edtMailCreateBcc.clear();
        if (task != 0) {
            if (resultList != null && resultList.size() > 0) {
                Set<PersonData> set = new LinkedHashSet<>(resultList);
                ArrayList<PersonData> uniqueList = new ArrayList<>(set);
                if (dataCreate != null) {
                    PersonData data = new PersonData(dataCreate.getFromName(), dataCreate.getFromAddr());
                    edtMailCreateTo.addObject(data);
                } else {
                    PersonData data = new PersonData(resultList.get(0).getFullName(), resultList.get(0).getEmail());
                    edtMailCreateTo.addObject(data);
                }
                if (task != 1) {
                    for (PersonData personData : uniqueList) {
/*                // in organization tree -> blue
                personData.setTypeColor(2);*/

                        switch (type) {
                            case Statics.ORGANIZATION_TO_ACTIVITY:
                                personData.setTypeAddress(0);
                                UserData userDto = UserData.getUserInformation();
                                if (!personData.getEmail().equals(userDto.getmEmail())) {
                                    edtMailCreateTo.addObject(personData);
                                }
                                break;
                            case Statics.ORGANIZATION_CC_ACTIVITY:
                                personData.setTypeAddress(1);
                                edtMailCreateCc.addObject(personData);
                                break;
                            case Statics.ORGANIZATION_BCC_ACTIVITY:
                                personData.setTypeAddress(2);
                                edtMailCreateBcc.addObject(personData);
                                break;
                        }

                    }
                }
                edtMailCreateTo.allowCollapse(false);
                if (resultList.size() == 1) {
                    edtMailCreateTo.allowCollapse(false);
                } else {
                    edtMailCreateTo.allowCollapse(true);
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

    public void BindData(MailBoxData data) {
        if (data != null && data.getMailNo() > 0) {
            edtMailCreateContent.requestFocus();
            txtMailCreateOriginalMessage.setVisibility(View.VISIBLE);
            txtMailCreateOriginalMessage.setText(MailHelper.getOriginalMessage(getActivity(), dataCreate));
            if (task == 0 || task == 3) {
                chkMailCreateQuote.setVisibility(View.GONE);
            } else {
                chkMailCreateQuote.setVisibility(View.VISIBLE);
            }
            webMailCreateContent.setVisibility(View.VISIBLE);
            edtMailCreateSubject.setText(data.getSubject());
            webMailCreateContent.loadData(data.getContentReply(),
                    "text/html; charset=utf-8", null);
            handleSelectedOrganizationResult(Statics.ORGANIZATION_TO_ACTIVITY, data.getListPersonDataTo());
            handleSelectedOrganizationResult(Statics.ORGANIZATION_CC_ACTIVITY, data.getListPersonDataCc());
            handleSelectedOrganizationResult(Statics.ORGANIZATION_BCC_ACTIVITY, data.getListPersonDataBcc());
            if (task == 3 || task == 0) {
                if (task == 3)
                    edtMailCreateContent.setText(Html.fromHtml(mailBoxData.getContent()));
                if (task == 0)
                    //imgAddAttach.setVisibility(View.GONE);
                    edtMailCreateTo.setText("");
                if (mailBoxData.getListAttachMent().size() > 0) {
                    linearAttach.setVisibility(View.VISIBLE);
                    for (final AttachData attachData : mailBoxData.getListAttachMent()) {
                        final AdapterMailCreateAttachLinear itemView = new AdapterMailCreateAttachLinear(getActivity(), attachData);
                        itemView.setTag(attachData);
                        itemView.getImageButtonDownLoad().setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        });
                        linearAttach.addView(itemView);
                    }
                } else {
                    linearAttach.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.mail_create_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                if (checkDraft(1)) {
                    DrafMail();
                } else {
                    getActivity().finish();
//                    AlertDialogView.normalAlertDialogWithCancel(getActivity(), getString(R.string.app_name), getString(R.string.string_email_cancel), getString(R.string.yes), getString(R.string.no), new AlertDialogView.OnAlertDialogViewClickEvent() {
//                        @Override
//                        public void onOkClick(DialogInterface alertDialog) {
//
//                        }
//
//                        @Override
//                        public void onCancelClick() {
//
//                        }
//                    });
                }
                break;
            case R.id.menuMailCreateSend:
                if (isSend) {
                    EventBus.getDefault().post(new CompleteTextView());
                    if (mailBoxData != null) {
                        isBack = true;
                        String subject = edtMailCreateSubject.getText().toString();
                        String content = edtMailCreateContent.getText().toString();
                        if (TextUtils.isEmpty(subject)) {
                            AlertDialogView.normalAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.string_lost_subjet), getString(R.string.string_title_mail_create_ok), null);
                            isSend = true;
//                        }
//                        else if (TextUtils.isEmpty(content)) {
//                            AlertDialogView.normalAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.string_lost_content), getString(R.string.string_title_mail_create_ok), null);
//                            isSend = true;
                        } else if (checkDraft(0)) {
                            if (task != 4) {
                                AlertDialogView.normalAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.string_title_mail_create_error), getString(R.string.string_title_mail_create_ok), null);
                                isSend = true;
                            } else {
                                PreferenceUtilities preferenceUtilities = DaZoneApplication.getInstance().getPreferenceUtilities();
                                String mailTo = preferenceUtilities.getIsMail();
                                int pos = mailTo.indexOf("@");
                                String username = "mdt";
                                if (pos != -1) {
                                    username = mailTo.substring(0, mailTo.indexOf("@"));
                                }
                                if (edtMailCreateTo.getObjects().size() == 0) {
                                    List<PersonData> listPer = new ArrayList<>();
                                    PersonData data = new PersonData();
                                    //   listPer.add(data);
                                    ArrayList<PersonData> list = new ArrayList<>();
                                    list.addAll(edtMailCreateBcc.getObjects());
                                    list.addAll(edtMailCreateCc.getObjects());
                                    // list.addAll(listPer);
                                    //  int temp = MailHelper.CheckEmail(list);
                                    int value = MailHelper.checkEmailV2(list);
                                    checkCaseErr(value, list, content, subject);
                                   /* if (temp == 0) {
                                        SendMail2(listPer, content, subject, 0);
                                    } else if (temp == 2) {
                                        AlertDialogView.normalAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.string_title_mail_create_error), getString(R.string.string_title_mail_create_ok), null);
                                    } else {
                                        Util.showMessage(getString(R.string.string_email_error_new));
                                    }
*/
                                } else {
//
                                    ArrayList<PersonData> list = new ArrayList<>();
                                    list.addAll(edtMailCreateBcc.getObjects());
                                    list.addAll(edtMailCreateCc.getObjects());
                                    list.addAll(edtMailCreateTo.getObjects());
                                    int value = MailHelper.checkEmailV2(list);
                                    checkCaseErr(value, null, content, subject);
                                   /* if (temp == 0) {
                                        SendMail(content, subject, 0);
                                    } else if (temp == 2) {
                                        AlertDialogView.normalAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.string_title_mail_create_error), getString(R.string.string_title_mail_create_ok), null);
                                    } else {
                                        Util.showMessage(getString(R.string.string_email_error_new));
                                    }*/
                                }

                            }

                        } else {
                            PreferenceUtilities preferenceUtilities = DaZoneApplication.getInstance().getPreferenceUtilities();
                            String mailTo = preferenceUtilities.getIsMail();
                            int pos = mailTo.indexOf("@");
                            String username = "mdt";
                            if (pos != -1) {
                                username = mailTo.substring(0, mailTo.indexOf("@"));
                            }
                            if (edtMailCreateTo.getObjects().size() == 0) {
                              /*  List<PersonData> listPer = new ArrayList<>();*/
                                PersonData data = new PersonData();
                                //  listPer.add(data);
                                ArrayList<PersonData> list = new ArrayList<>();
                                list.addAll(edtMailCreateBcc.getObjects());
                                list.addAll(edtMailCreateCc.getObjects());
                                // list.addAll(listPer);
                                // int temp = MailHelper.CheckEmail(list);
                                int value = MailHelper.checkEmailV2(list);
                                checkCaseErr(value, list, content, subject);
                             /*   if (temp == 0) {
                                    SendMail2(listPer, content, subject, 0);
                                } else if (temp == 2) {
                                    AlertDialogView.normalAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.string_title_mail_create_error), getString(R.string.string_title_mail_create_ok), null);
                                } else {
                                    Util.showMessage(getString(R.string.string_email_error_new));
                                }*/

                            } else {
//
                                ArrayList<PersonData> list = new ArrayList<>();
                                list.addAll(edtMailCreateBcc.getObjects());
                                list.addAll(edtMailCreateCc.getObjects());
                                list.addAll(edtMailCreateTo.getObjects());
                                int value = MailHelper.checkEmailV2(list);
                                checkCaseErr(value, null, content, subject);

                                /*if (temp == 0) {
                                    SendMail(content, subject, 0);
                                } else if (temp == 2) {
                                    AlertDialogView.normalAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.string_title_mail_create_error), getString(R.string.string_title_mail_create_ok), null);
                                } else {
                                    Util.showMessage(getString(R.string.string_email_error_new));
                                }*/
                            }
                        }
                    }
                }

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkCaseErr(int errValue, List<PersonData> listPer, String content, String subject) {
        /*if (mailBoxDataNew.getListAttachMent().size() > 0) {
            SendMailNewDarft(content, subject, 0);
        } else {*/
        if (errValue == 0) {
            AlertDialogView.normalAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.string_title_mail_create_error), getString(R.string.string_title_mail_create_ok), null);
        } else if (errValue == 1) {
            Util.showMessage(getString(R.string.string_email_error_new));
        } else if (errValue == 2) {
            if (listPer == null) {
                SendMail(content, subject, 0);
            } else {
                SendMail2(listPer, content, subject, 0);
            }
        }
    }
    // }

    private boolean checkDraft1(int i) {
        if (i == 0) {
            return true;
        }
        return true;
    }

    private void SendMail2(List<PersonData> listPer, String content, String subject, int isTask) {
        isSend = false;
        String dataContent = "";
        dataContent = content;
        //}
        mailBoxData.setListPersonDataBcc(new ArrayList<PersonData>());
        mailBoxData.setListPersonDataCc(new ArrayList<PersonData>());
        mailBoxData.setListPersonDataTo(new ArrayList<PersonData>());
        mailBoxData.getListPersonDataBcc().addAll(edtMailCreateBcc.getObjects());
        mailBoxData.getListPersonDataTo().addAll(listPer);
        mailBoxData.getListPersonDataCc().addAll(edtMailCreateCc.getObjects());
        mailBoxData.setContent(dataContent);
        mailBoxData.setSubject(subject);
        mailBoxData.setPriority("3");
        if (task == 0)
            if (dataContent != null) {
                if (tp != null && tp.size() > 0)
                    for (AttachData attachData : tp)
                        mailBoxData.getListAttachMent().remove(attachData);
            }
        if ((isUpload + isFail) == mailBoxData.getListAttachMent().size()) {
            if (isTask == 0) {
                if (task == 4 || task == 3) {
                    MailNo = mailBoxData.getMailNo();
                    mailBoxData.setMailNo(0);
                    HttpRequest.getInstance().ComposeMail(mailBoxData, new BaseHTTPCallBack() {
                        @Override
                        public void onHTTPSuccess() {
                            HttpRequest.getInstance().deleteEmail(MailNo, new BaseHTTPCallBack() {
                                @Override
                                public void onHTTPSuccess() {
                                    if (isVisible()) {
//                                        Intent intent = new Intent(getActivity(), ListEmailActivity.class);
//                                        intent.putExtra("LIST_MAIL", 1);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        startActivity(intent);
                                        Util.showMessage(getString(R.string.string_alert_send_mail_success));
                                        EventBus.getDefault().post(new NewMailEvent("1"));
                                        isSendMail = true;
                                        Intent intent = new Intent(getContext(), ListEmailActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        getActivity().finish();


//                                        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                    }
                                }

                                @Override
                                public void onHTTPFail(ErrorData errorDto) {
                                    if (isVisible()) {
                                        Util.showMessageShort(errorDto.getMessage());
                                        getActivity().finish();
                                    }
                                }
                            });
                        }

                        @Override
                        public void onHTTPFail(ErrorData errorDto) {
                            if (isVisible()) {
                                Util.showMessageShort(errorDto.getMessage());
                                //getActivity().finish();
                            }
                        }
                    });
                } else if (task == 0) {
                    HttpRequest.getInstance().ForwardMail(mailBoxData, this);
                } else if (task == 1 || task == 2) {
                    HttpRequest.getInstance().ReplyMail(mailBoxData, this);
                }
            } else if (isTask == 1) {
                HttpRequest.getInstance().DrafComposeMail(mailBoxData, FragmentMailCreate.this);
            } else if (isTask == 2) {
                HttpRequest.getInstance().DrafReplyMail(mailBoxData, FragmentMailCreate.this);
            } else {
                HttpRequest.getInstance().DrafForwardMail(mailBoxData, FragmentMailCreate.this);
            }
        } else {
            String notice = getString(R.string.string_alert_send_mail_fail);
            Util.showMessage(notice);
            isSend = true;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == Activity.RESULT_OK) {
                switch (requestCode) {
                    case FILE_PICKER_SELECT:
                        if (data.getBooleanExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false)) {
                            // For JellyBean and above
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                ClipData clip = data.getClipData();

                                if (clip != null) {
                                    for (int i = 0; i < clip.getItemCount(); i++) {
                                        Uri uri = clip.getItemAt(i).getUri();
                                        GetFile(uri);
                                        // Do something with the URI
                                    }
                                }
                                // For Ice Cream Sandwich
                            } else {
                                ArrayList<String> paths = data.getStringArrayListExtra
                                        (FilePickerActivity.EXTRA_PATHS);

                                if (paths != null) {
                                    for (String path : paths) {
                                        Uri uri = Uri.parse(path);
                                        // Do something with the URI
                                        GetFile(uri);
                                    }
                                }
                            }

                        } else {
                            Uri uri = data.getData();
                            GetFile(uri);
                            // Do something with the URI
                        }
                        break;
                    case Statics.ORGANIZATION_TO_ACTIVITY:
                    case Statics.ORGANIZATION_CC_ACTIVITY:
                    case Statics.ORGANIZATION_BCC_ACTIVITY:
                        ArrayList<PersonData> resultList = data.getExtras().getParcelableArrayList(StaticsBundle.BUNDLE_LIST_PERSON);
                        handleSelectedOrganizationResult(requestCode, resultList);
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onTokenAdded(Object token) {
        if (edtMailCreateTo.getObjects() != null && edtMailCreateTo.getObjects().size() > 1) {
            edtMailCreateTo.allowCollapse(true);
        }
    }

    @Override
    public void onTokenRemoved(Object token) {

    }

    @Override
    public void onClick(View v) {
        ArrayList<PersonData> selectedList = new ArrayList<>();
        switch (v.getId()) {
            case R.id.imgMailCreateOrgTo:
                selectedList.addAll(edtMailCreateTo.getObjects());
                startOrganizationActivity(selectedList, Statics.ORGANIZATION_TO_ACTIVITY);
                break;
            case R.id.imgMailCreateOrgCc:
                selectedList.addAll(edtMailCreateCc.getObjects());
                startOrganizationActivity(selectedList, Statics.ORGANIZATION_CC_ACTIVITY);
                break;

            case R.id.imgMailCreateOrgBcc:
                selectedList.addAll(edtMailCreateBcc.getObjects());
                startOrganizationActivity(selectedList, Statics.ORGANIZATION_BCC_ACTIVITY);
                break;
        }
    }


    private void startOrganizationActivity(ArrayList<PersonData> selectedList, int type) {
        Log.d(TAG, "startOrganizationActivity");
        Intent i = new Intent(getActivity(), OrganizationActivity.class);
//        i.putParcelableArrayListExtra(StaticsBundle.BUNDLE_LIST_PERSON, selectedList);
//        startActivityForResult(i, type);
        i.putExtra("TYPE", type);
        startActivity(i);

//        startActivity(new Intent(getActivity(), OrganizationActivity.class));
    }

    //isTask: 0-Compose 1-DrafCompose
    public void SendMail(String content, String subject, int isTask) {
        isSend = false;
        String dataContent = "";
        dataContent = content;
        //}
        mailBoxData.setListPersonDataBcc(new ArrayList<PersonData>());
        mailBoxData.setListPersonDataCc(new ArrayList<PersonData>());
        mailBoxData.setListPersonDataTo(new ArrayList<PersonData>());
        mailBoxData.getListPersonDataBcc().addAll(edtMailCreateBcc.getObjects());
        mailBoxData.getListPersonDataTo().addAll(edtMailCreateTo.getObjects());
        mailBoxData.getListPersonDataCc().addAll(edtMailCreateCc.getObjects());
        mailBoxData.setContent(dataContent);
        mailBoxData.setSubject(subject);
        mailBoxData.setPriority("3");

        if (task == 0)
            if (dataContent != null) {
                if (tp != null && tp.size() > 0)
                    for (AttachData attachData : tp)
                        mailBoxData.getListAttachMent().remove(attachData);
            }
        if ((isUpload + isFail) == mailBoxData.getListAttachMent().size()) {
            mailBoxData.getListAttachMent().clear();
            mailBoxData.getListAttachMent().addAll(mailBoxDataNew.getListAttachMent());
            if (isTask == 0) {
                if (task == 4 || task == 3) {
                    MailNo = mailBoxData.getMailNo();
                    mailBoxData.setMailNo(0);
                    HttpRequest.getInstance().ComposeMail(mailBoxData, new BaseHTTPCallBack() {
                        @Override
                        public void onHTTPSuccess() {
                            HttpRequest.getInstance().deleteEmail(MailNo, new BaseHTTPCallBack() {
                                @Override
                                public void onHTTPSuccess() {
                                    if (isVisible()) {
//                                        Intent intent = new Intent(getActivity(), ListEmailActivity.class);
//                                        intent.putExtra("LIST_MAIL", 1);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        startActivity(intent);
                                        Util.showMessage(getString(R.string.string_alert_send_mail_success));
                                        EventBus.getDefault().post(new NewMailEvent("1"));
                                        isSendMail = true;
                                        Intent intent = new Intent(getContext(), ListEmailActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        getActivity().finish();
//                                        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                    }
                                }

                                @Override
                                public void onHTTPFail(ErrorData errorDto) {
                                    if (isVisible()) {
                                        Util.showMessageShort(errorDto.getMessage());
                                        getActivity().finish();
                                    }
                                }
                            });
                        }

                        @Override
                        public void onHTTPFail(ErrorData errorDto) {
                            if (isVisible()) {
                                Util.showMessageShort(errorDto.getMessage());
                                //getActivity().finish();
                            }
                        }
                    });
                } else if (task == 0) {
                    HttpRequest.getInstance().ForwardMail(mailBoxData, this);
                } else if (task == 1 || task == 2) {
                    HttpRequest.getInstance().ReplyMail(mailBoxData, this);
                }
            } else if (isTask == 1 || isTask == 3) {
                HttpRequest.getInstance().DrafComposeMail(mailBoxData, FragmentMailCreate.this);
            } else if (isTask == 2) {
                HttpRequest.getInstance().DrafReplyMail(mailBoxData, FragmentMailCreate.this);
            } else {
                HttpRequest.getInstance().DrafForwardMail(mailBoxData, FragmentMailCreate.this);
            }
        } else {
            String notice = getString(R.string.string_alert_send_mail_fail);
            Util.showMessage(notice);
            isSend = true;
        }
    }

    public void SendMailNewDarft(String content, String subject, int isTask) {
        isSend = false;
        String dataContent = "";
        dataContent = content;
        //}
        mailBoxDataNew.setListPersonDataBcc(new ArrayList<PersonData>());
        mailBoxDataNew.setListPersonDataCc(new ArrayList<PersonData>());
        mailBoxDataNew.setListPersonDataTo(new ArrayList<PersonData>());
        mailBoxDataNew.getListPersonDataBcc().addAll(edtMailCreateBcc.getObjects());
        mailBoxDataNew.getListPersonDataTo().addAll(edtMailCreateTo.getObjects());
        mailBoxDataNew.getListPersonDataCc().addAll(edtMailCreateCc.getObjects());
        mailBoxDataNew.setContent(dataContent);
        mailBoxDataNew.setSubject(subject);
        mailBoxDataNew.setPriority("3");
        if (task == 0)
            if (dataContent != null) {
                if (tp != null && tp.size() > 0)
                    for (AttachData attachData : tp)
                        mailBoxDataNew.getListAttachMent().remove(attachData);
            }
        if ((isUpload + isFail) == mailBoxDataNew.getListAttachMent().size()) {
            if (isTask == 0) {
                if (task == 4 || task == 3) {
                    MailNo = mailBoxDataNew.getMailNo();
                    mailBoxDataNew.setMailNo(0);
                    HttpRequest.getInstance().ComposeMail(mailBoxDataNew, new BaseHTTPCallBack() {
                        @Override
                        public void onHTTPSuccess() {
                            HttpRequest.getInstance().deleteEmail(MailNo, new BaseHTTPCallBack() {
                                @Override
                                public void onHTTPSuccess() {
                                    if (isVisible()) {
//                                        Intent intent = new Intent(getActivity(), ListEmailActivity.class);
//                                        intent.putExtra("LIST_MAIL", 1);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        startActivity(intent);
                                        Util.showMessage(getString(R.string.string_alert_send_mail_success));
                                        EventBus.getDefault().post(new NewMailEvent("1"));
                                        isSendMail = true;
                                        Intent intent = new Intent(getContext(), ListEmailActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        getActivity().finish();
//                                        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                    }
                                }

                                @Override
                                public void onHTTPFail(ErrorData errorDto) {
                                    if (isVisible()) {
                                        Util.showMessageShort(errorDto.getMessage());
                                        getActivity().finish();
                                    }
                                }
                            });
                        }

                        @Override
                        public void onHTTPFail(ErrorData errorDto) {
                            if (isVisible()) {
                                Util.showMessageShort(errorDto.getMessage());
                                //getActivity().finish();
                            }
                        }
                    });
                } else if (task == 0) {
                    HttpRequest.getInstance().ForwardMail(mailBoxData, this);
                } else if (task == 1 || task == 2) {
                    HttpRequest.getInstance().ReplyMail(mailBoxData, this);
                }
            } else if (isTask == 1) {
                HttpRequest.getInstance().DrafComposeMail(mailBoxData, FragmentMailCreate.this);
            } else if (isTask == 2) {
                HttpRequest.getInstance().DrafReplyMail(mailBoxData, FragmentMailCreate.this);
            } else {
                HttpRequest.getInstance().DrafForwardMail(mailBoxData, FragmentMailCreate.this);
            }
        } else {
            String notice = getString(R.string.string_alert_send_mail_fail);
            Util.showMessage(notice);
            isSend = true;
        }
    }

    //task : 0 - Create, reply, forward
    //1 - draft
    public boolean checkDraft(int task) {
        String subject = edtMailCreateSubject.getText().toString();
        String content = edtMailCreateContent.getText().toString();

        if (this.task == 4) {
            if (edtMailCreateTo.getObjects().size() != 0 && edtMailCreateBcc.getObjects().size() != 0 && edtMailCreateCc.getObjects().size() != 0) {
                return false;
            } else {
                return !(TextUtils.isEmpty(content) && attachData == null && TextUtils.isEmpty(subject));
            }
        } else if (this.task == 0) {
            if ((edtMailCreateTo.getObjects().size() != 0)) {
                if (task == 0) {
                    return false;
                }
                return true;
            } else {
                return !(TextUtils.isEmpty(content) && attachData == null);
            }
        } else if (this.task == 3) {
            if ((edtMailCreateTo.getObjects().size() != 0)) {
                if (task == 0) {
                    return false;
                }
                return true;
            } else {
                return !(TextUtils.isEmpty(content) && attachData == null);
            }
        } else {
            return task != 0 && !(TextUtils.isEmpty(content) && attachData == null);

        }
    }

    public void DrafMail() {
        final List<String> itemList = new ArrayList<>();
        itemList.add(getString(R.string.string_save_draft));
        if (task == 3)
            itemList.add(getString(R.string.string_delete_draft));
        else {
            itemList.add(getString(R.string.string_discard_draft));
        }
        MailHelper.showConfirmDialogYesNo(getActivity(), getString(R.string.content_draft), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String subject = edtMailCreateSubject.getText().toString();
                String content = edtMailCreateContent.getText().toString();
                if (task == 4) {
                    Log.d("sssDraft", "task4");
                    SendMail(content, subject, 1);
                } else if (task == 0) {
                    Log.d("sssDraft", "task0");
                    SendMail(content, subject, 3);
                } else if (task == 2) {
                    Log.d("sssDraft", "task1/2");
                    SendMail(content, subject, 2);
                } else if (task == 1 || task == 3) {
                   /* Log.d("sssDraft", "task3");
                    getActivity().finish();*/
                    SendMail(content, subject, 3);
                }
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
               /* if (task == 3)
                    HttpRequest.getInstance().moveEmailToTrash(mailBoxData.getMailNo(), false, new BaseHTTPCallBack() {
                        @Override
                        public void onHTTPSuccess() {
                            new Prefs().putBooleanValue(StaticsBundle.PREFS_KEY_RELOAD_LIST, true);
                            if (isVisible()) {
                                getActivity().finish();
                            }
                        }

                        @Override
                        public void onHTTPFail(ErrorData errorDto) {
                            Util.showMessageShort(getString(R.string.string_update_fail));
                        }
                    });
                else {
                    getActivity().finish();
                }*/
            }
        });
      /*  MailHelper.displaySingleChoiceListString(getActivity(), itemList, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (itemList.get(which).equalsIgnoreCase(getString(R.string.string_save_draft))) {
                    String subject = edtMailCreateSubject.getText().toString();
                    String content = edtMailCreateContent.getText().toString();
                    if (task == 4) {
                        SendMail(content, subject, 1);
                    } else if (task == 0) {
                        SendMail(content, subject, 3);
                    } else if (task == 1 || task == 2) {
                        SendMail(content, subject, 2);
                    } else if (task == 3) {
                        getActivity().finish();
                    }
                } else {
                    if (task == 3)
                        HttpRequest.getInstance().moveEmailToTrash(mailBoxData.getMailNo(), false, new BaseHTTPCallBack() {
                            @Override
                            public void onHTTPSuccess() {
                                new Prefs().putBooleanValue(StaticsBundle.PREFS_KEY_RELOAD_LIST, true);
                                if (isVisible()) {
                                    getActivity().finish();
                                }
                            }

                            @Override
                            public void onHTTPFail(ErrorData errorDto) {
                                Util.showMessageShort(getString(R.string.string_update_fail));
                            }
                        });
                    else {
                        getActivity().finish();
                    }
                }
            }
        }, getString(R.string.app_name));*/
    }

    @Override
    public void OnMailDetailSuccess(MailBoxData mailBoxData) {
        if (isVisible()) {
            if (mailBoxData != null)
                this.mailBoxData = mailBoxData;
            BindData(mailBoxData);
        }
    }

    @Override
    public void OnMaillDetailFail(ErrorData errorData) {
        if (isVisible()) {
            Util.showMessageShort(errorData.getMessage());
        }
    }

    @Override
    public void onGetAllOfUserSuccess(ArrayList<PersonData> list) {
        ArrayList<PersonData> test = new ArrayList<>();
        if (list != null) {
            for (PersonData personData : list) {
                if (personData.getFullName().equalsIgnoreCase(UserData.getUserInformation().getFullName())) {

                } else {
                    people.add(personData);
                }
            }
        }
    }

    @Override
    public void onGetAllOfUserFail(ErrorData errorData) {
    }

    @Override
    public void onHTTPSuccess() {
        if (isBack) {
            isBack = false;
            Util.showMessage(getString(R.string.string_alert_send_mail_success));
            isSendMail = true;
        } else {
            Util.showMessage(getString(R.string.string_alert_save_mail_success));
        }
        if (isVisible()) {
//            long temp = new Prefs().getLongValue(StaticsBundle.PREFS_KEY_COMPOSE, 0);
//            if (temp != 0) {
//                new Prefs().putLongValue(Statics.SAVE_BOX_NO_PREF, temp);
//            }

            Intent intent = new Intent(getActivity(), ListEmailActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            if (ActivityMailCreate.Instance != null) {
                ActivityMailCreate.Instance.finish();
            }
        }
    }

    @Override
    public void onHTTPFail(ErrorData errorDto) {
        Util.showMessage(getString(R.string.string_alert_send_mail_fail));
        isSend = true;
    }

    @Override
    public void OnGetListOfMailAccountSuccess(ArrayList<AccountData> accountData) {
        if (accountData != null) {
            listUser.addAll(accountData);
            if (adapterMailCreateFromSpiner != null)
                adapterMailCreateFromSpiner.notifyDataSetChanged();
        }
    }

    @Override
    public void OnGetListOfMailAccountFail(ErrorData errorData) {

    }

    @Subscribe
    public void onEvent(EventRequest eventPersion) {
        if (eventPersion != null) {
            for (PersonData data : eventPersion.getDatas()) {
                switch (eventPersion.getType()) {
                    case Statics.ORGANIZATION_TO_ACTIVITY:
                        edtMailCreateTo.addObject(data);
                        mailBoxData.getListPersonDataTo().addAll(edtMailCreateTo.getObjects());
                        break;
                    case Statics.ORGANIZATION_BCC_ACTIVITY:
                        edtMailCreateBcc.addObject(data);
                        mailBoxData.getListPersonDataBcc().addAll(edtMailCreateBcc.getObjects());
                        break;
                    case Statics.ORGANIZATION_CC_ACTIVITY:
                        edtMailCreateCc.addObject(data);
                        mailBoxData.getListPersonDataCc().addAll(edtMailCreateCc.getObjects());
                        break;

                }

            }

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);

    }

    @Override
    public void push(double percent, String nameFile) {
        // txtPercentage.setText(""+(int)percent);

        if (mProgressBar != null) {
            this.fileName = nameFile;
            mProgressBar.setProgress((int) percent);

        }
    }

    protected String handleBackground(String... params) {
        String filePath = params[0];
        String fileName = params[1];

        return fileUploadDownload.uploadFileToServer(filePath, fileName, this);

    }

    private void updateTextView() {
        txtPercentage.setText(countFile + " / " + mailBoxData.getListAttachMent().size());
    }

    private class Upload extends UploadFileToServer {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
            txtPercentage.setVisibility(View.VISIBLE);
            countFile = isUpload + 1;
            updateTextView();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Gson gson = new Gson();
            List<AttachData> list = new ArrayList<>();
            try {
                JSONObject jo = new JSONObject(result);
                String namefile = jo.getString("name");
                boolean status = jo.getBoolean("success");
                long sizefile = jo.getLong("size");
                AttachData attachData = new AttachData();
                attachData.setFileName(namefile);
                attachData.setFileSize(sizefile);
                list.add(attachData);
                mailBoxDataNew.getListAttachMent().addAll(list);
                if (status) {
                    isUpload++;
                    int countFile = isUpload + 1;
                    txtPercentage.setText(countFile + " / " + mailBoxData.getListAttachMent().size());
                    if (isUpload == mailBoxData.getListAttachMent().size()) {
                        mProgressBar.setVisibility(View.GONE);
                        if (progress_bar != null) {
                            progress_bar.setVisibility(View.GONE);
                        }
                        txtPercentage.setVisibility(View.GONE);
                    }
                } else {
                    isFail++;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected String doInBackground(String... params) {
            String filePath = params[0];
            String fileName = params[1];
            return handleBackground(params);
        }
    }

}
