package com.dazone.crewemail.dto.Tree;


import com.dazone.crewemail.data.PersonData;
import com.dazone.crewemail.dto.Tree.Dtos.TreeUserDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class Org_tree {

    public static PersonData buildTree(List<PersonData> dtos) throws Exception {
        //boolean temp = true;
        PersonData root = new PersonData();
        HashMap<Integer, ArrayList<PersonData>> jsonDataMap = new HashMap<>();
        //HashMap<Integer, ArrayList<TreeUserDTO>> jsonDataMap2 = new HashMap<>();
        for (PersonData dto : dtos) {
            if (jsonDataMap.containsKey(dto.getDepartmentParentNo())) {
                if (dto.getType() == 2) {
                    ArrayList<PersonData> currentList = jsonDataMap.get(dto.getDepartmentParentNo());
                    // Collection sort here --> Check dutyNo
                    currentList.add(dto);
//                    // sort data by order
//                    Collections.sort(currentList, new Comparator<PersonData>() {
//                        @Override
//                        public int compare(PersonData r1, PersonData r2) {
//                            if (r1.getPositionSortNo() > r2.getPositionSortNo()) {
//                                return 1;
//                            }else if (r1.getPositionSortNo() == r2.getPositionSortNo()) {
//                                return 0;
//                            } else {
//                                return -1;
//                            }
//                        }
//                    });
                } else {
                    jsonDataMap.get(dto.getDepartmentParentNo()).add(dto);
                }
            } else {
                ArrayList<PersonData> subordinates = new ArrayList<>();
                subordinates.add(dto);
                jsonDataMap.put(dto.getDepartmentParentNo(), subordinates);
            }
        }
//        for (PersonData subordinate : jsonDataMap.get(root.getDepartNo())) {
//            root.addSubordinate(subordinate);
//            subordinate.setDepartmentParentNo(root.getDepartNo());
//            // Sort by name
//            buildSubTree(subordinate, jsonDataMap);
//        }
        return root;
    }

    private static void buildSubTree(PersonData parent, HashMap<Integer, ArrayList<PersonData>> jsonDataMap) {
        List<PersonData> subordinates = jsonDataMap.get(parent.getDepartNo());
        if (subordinates != null) {
            for (PersonData subordinate : subordinates) {
                subordinate.setDepartmentParentNo(parent.getDepartmentParentNo());

//                parent.addSubordinate(subordinate);
                if (subordinate.getType() == 0) {
                    buildSubTree(subordinate, jsonDataMap);
                }
            }
        }
    }

}
