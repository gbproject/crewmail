package com.dazone.crewemail.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dazone.crewemail.R;
import com.dazone.crewemail.activities.BaseActivity;
import com.dazone.crewemail.data.AttachData;
import com.dazone.crewemail.utils.Util;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Jerry on 11/28/2017.
 */

public class AttachAdapter extends BaseAdapter<AttachData, AttachAdapter.ViewHolder> {


    public AttachAdapter(Activity mActivity) {
        super(mActivity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_mail_create_item_attach, parent, false);
        ButterKnife.bind(v);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.txtMailCreateItemAttachFileName)
        TextView txtMailCreateAttachFileName;

        @Bind(R.id.txtMailCreateItemAttachFileSize)
        TextView txtMailCreateAttachFileSize;

        @Bind(R.id.imgMailCreateItemAttachDownload)
        ImageButton imgMailCreateItemAttachDownload;

        @Bind(R.id.imgMailCreateItemAttachDelete)
        ImageButton imgMailCreateItemAttachDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imgMailCreateItemAttachDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //remove
                    onClicklistener.onRemove(getAdapterPosition());
                }
            });

            imgMailCreateItemAttachDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClicklistener.onDownload(getAdapterPosition());
                }
            });
        }

        void bind(int pos) {
            AttachData attachData = list.get(pos);
            txtMailCreateAttachFileName.setText(attachData.getFileName());
            txtMailCreateAttachFileSize.setText(" (" + Util.readableFileSize(attachData.getFileSize()) + ")");
        }
    }

    public interface onClicklistener {
        void onRemove(int pos);

        void onDownload(int pos);
    }

    private onClicklistener onClicklistener;

    public void setOnClicklistener(AttachAdapter.onClicklistener onClicklistener) {
        this.onClicklistener = onClicklistener;
    }
}
